# Erkapharm testproject

## Запуск проекта
### `npm run server`
Запуск локального сервера с рандомной генерацией ошибки
### `npm start`
Запуск React приложения

#### `либо`

### `npm run serv`
Запуск локального сервера без рандомной генерации ошибки
### `npm start`
Запуск React приложения

## Использованные плагины
- "node-sass": "^5.0.0",
- "react": "^17.0.2",
- "react-dom": "^17.0.2",
- "react-redux": "^7.2.3",
- "react-scripts": "4.0.3",
- "redux": "^4.0.5",
- "redux-devtools-extension": "^2.13.9",
- "redux-thunk": "^2.3.0",
- "reset-css": "^5.0.1",
