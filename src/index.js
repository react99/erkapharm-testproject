import 'reset-css';

import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from "react-redux";
import {createGlobalStyle} from "styled-components";

import App from './components/App';
import {store} from "./store/store";

const GlobalStyled = createGlobalStyle`
  * {
    box-sizing: border-box;
    font-family: Arial, sans-serif;
    font-size: 14px;
    font-weight: 400;
  }
`

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <GlobalStyled/>
            <App/>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);

