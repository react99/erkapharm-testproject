import React, {useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import styled from "styled-components";

import {faArrowAltCircleLeft} from "@fortawesome/free-regular-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

import {getCurrentStore} from "../api/api";

export const DetailStore = ({history}) => {
    const {id_store} = useParams()
    const [currentStore, setCurrentStore] = useState({})

    useEffect(() => {
        getCurrentStore(id_store, setCurrentStore)
    }, []) // eslint-disable-line react-hooks/exhaustive-deps

    return (
        <Wrapper className="detail-store">
            <Content>
                <Row>
                    <FieldName>Магазин:</FieldName>
                    <FieldText>{currentStore.name}</FieldText>
                </Row>
                <Row>
                    <FieldName>Адрес:</FieldName>
                    <FieldText>{currentStore.address?.address}</FieldText>
                </Row>
                <Row>
                    <FieldName>Время работы:</FieldName>
                    <FieldText>{currentStore.workTime?.descr}</FieldText>
                </Row>
                <Row>
                    <FieldName>Координаты:</FieldName>
                    <FieldText>
                        <a href={`https://yandex.ru/maps/?pt=${currentStore.address?.coordinates.lon},${currentStore.address?.coordinates.lat}&z=15&l=map`}
                           target="_blank" rel="noreferrer">
                            {currentStore.address?.coordinates.lon}, {currentStore.address?.coordinates.lat}
                        </a>
                    </FieldText>
                </Row>
            </Content>
            <ButtonBack onClick={history.goBack}>
                <Icon icon={faArrowAltCircleLeft} />
            </ButtonBack>
        </Wrapper>
    )
}

const Wrapper = styled.div`
  margin-top: 20px;
  position: relative;
`
const Content = styled.div`
  border: 2px solid lightsalmon;
  border-radius: 10px;
  padding: 10px;
`
const Row = styled.div`
  display: flex;
  padding: 5px 0;
`
const FieldName = styled.div`
  padding: 7px;
  width: 18%;
`
const FieldText = styled.div`
  padding: 7px;
  width: 82%;
`
const ButtonBack = styled.div`
  position: absolute;
  top: 10px;
  right: 10px;
  cursor: pointer;
`
const Icon = styled(FontAwesomeIcon)`
  font-size: 20px;
  color: lightsalmon;

  &:hover {
    color: lightslategrey;
  }
`