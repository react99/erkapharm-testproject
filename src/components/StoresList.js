import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import styled from "styled-components";

import {StoreItem} from "./StoreItem";
import {getStores} from "../api/api";

export const StoresList = () => {
    const dispatch = useDispatch();
    const storesList = useSelector(state => state.stores.storesList)

    useEffect(() => {
        dispatch(getStores());
    }, []) // eslint-disable-line react-hooks/exhaustive-deps

    return (
        <List>
            { storesList.map(item => <StoreItem key={item.id} storeItem={item} />) }
        </List>
    )
}

const List = styled.div`
  display: flex;
  flex-direction: column;
`