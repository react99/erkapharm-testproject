import React from "react";
import {useSelector} from "react-redux";
import {BrowserRouter, Switch, Route, Redirect} from "react-router-dom";
import styled from "styled-components";

import {StoresList} from "./StoresList";
import {DetailStore} from "./DetailStore";

function App() {
    const error = useSelector(state => state.stores.fetchError)

    return (
        <Container>
            <ErrorBlock active={error.status}>
                <ErrorMessage>{error.msg}</ErrorMessage>
            </ErrorBlock>
            <BrowserRouter>
                <Switch>
                    <Route exact path="/" component={StoresList}/>
                    <Route path="/store/:id_store" component={DetailStore}/>
                    <Redirect to="/"/>
                </Switch>
            </BrowserRouter>
        </Container>
    );
}

const Container = styled.div`
  max-width: 1080px;
  margin: 0 auto;
  padding: 0 20px;
`

const ErrorMessage = styled.div`
  text-align: center;
  margin: 0 auto;
  font-size: 1.4rem;
  text-transform: uppercase;
  color: #e6f1f1;
  line-height: 45px;
`

const ErrorBlock = styled.div`
  position: fixed;
  z-index: 100;
  top: 0;
  left: 0;
  height: 45px;
  width: 100vw;
  background-color: #d00909;
  transform: ${({active}) => !active ? 'scaleY(0)' : 'scaleY(1)'};
  transform-origin: 0 0 0;
  transition: transform 0.5s;
`

export default App;
