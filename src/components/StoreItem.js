import React, {useState} from "react";
import {useDispatch} from "react-redux";
import {Link} from "react-router-dom";
import styled from "styled-components";
import {faTrashAlt, faEdit, faSave, faEye} from "@fortawesome/free-regular-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

import {deleteStore, updateStore} from "../api/api";

export const StoreItem = ({storeItem}) => {
    const [inputReadOnly, setInputReadOnly] = useState(true)
    const [itemState, setItemState] = useState(storeItem)
    const dispatch = useDispatch()

    const editHandler = () => {
        if (inputReadOnly) {
            setInputReadOnly(false);
            return;
        }
        dispatch(updateStore(itemState, setInputReadOnly))
    }

    const deleteHandler = () => {
        dispatch(deleteStore(itemState.id))
    }

    const changeHandlerName = (e) => {
        setItemState({...itemState, name: e.target.value})
    }
    const changeHandlerAddress = (e) => {
        setItemState({...itemState, address: {address: e.target.value}})
    }
    const changeHandlerDesc = (e) => {
        setItemState({...itemState, workTime: {descr: e.target.value}})
    }

    return (
        <ItemWrapper>
            <div>
                <Row>
                    <FieldName>Магазин:</FieldName>
                    <FieldText readOnly={inputReadOnly}
                               value={itemState.name}
                               onChange={changeHandlerName}
                               width="72%"

                    />
                </Row>
                <Row>
                    <FieldName>Адрес:</FieldName>
                    <FieldText readOnly={inputReadOnly} value={itemState.address.address}
                               onChange={changeHandlerAddress}/>
                </Row>
                <Row>
                    <FieldName>Время работы:</FieldName>
                    <FieldText readOnly={inputReadOnly} value={itemState.workTime.descr} onChange={changeHandlerDesc}/>
                </Row>
            </div>
            <ButtonsWrapper>
                <Button onClick={editHandler}>
                    {inputReadOnly ? <Icon icon={faEdit}/> : <Icon icon={faSave}/>}
                </Button>
                <Button onClick={deleteHandler}>
                    <Icon icon={faTrashAlt}/>
                </Button>
                <Button>
                    <Link to={`/store/${itemState.id}`}>
                        <Icon icon={faEye}/>
                    </Link>
                </Button>
            </ButtonsWrapper>
        </ItemWrapper>
    )
}

const ItemWrapper = styled.div`
  border: 2px solid lightsalmon;
  border-radius: 10px;
  margin: 4px 0;
  position: relative;
  padding: 10px;
`

const Row = styled.div`
  display: flex;
  padding: 5px 0;
`

const FieldName = styled.div`
  padding: 7px;
  width: 18%;
`
const FieldText = styled.textarea`
  padding: 7px;
  border: none;
  outline: none;
  width: ${props => props.width || '82%'};
  resize: none;
  background-color: #FFA07A31;

  &:read-only {
    cursor: default;
    background-color: transparent;
  }
`

const ButtonsWrapper = styled.div`
  position: absolute;
  display: flex;
  justify-content: space-between;
  top: 10px;
  right: 10px;
`

const Button = styled.div`
  margin: 0 5px;
  cursor: pointer;
`

const Icon = styled(FontAwesomeIcon)`
  font-size: 20px;
  color: lightsalmon;

  &:hover {
    color: lightslategrey;
  }
`