import {setStoresAction, deleteStoreAction, updateStoreAction, setFetchError} from "../reducers/storesReducer";

const url = new URL('http://localhost:5000')

const errorHandler = (dispatch, objState) => {
	dispatch(setFetchError(objState))
	setTimeout(() => {
		dispatch(setFetchError({status: false, msg: ''}))
	}, 2000)
}

export const getStores = () => dispatch => {
	const getStoresUrl = new URL('/stores', url)

	fetch(getStoresUrl.toString(), {
		method: 'GET'
	}).then(response => response.json())
		.then(data => dispatch(setStoresAction(data)))
}

export const getCurrentStore = (id, callback) => {
	const getStoreUrl = new URL(`/stores/${id}`, url)

	fetch(getStoreUrl.toString(), {
		method: 'GET'
	}).then(response => response.json())
		.then(data => callback(data))
}

export const deleteStore = (id) => dispatch => {
	const deleteStoreUrl = new URL(`/stores/${id}`, url);

	fetch(deleteStoreUrl.toString(), {
		method: 'DELETE'
	}).then(response => {
		if (response.status === 200) {
			dispatch(deleteStoreAction(id));
			return;
		}
		throw new Error('Невозможно удалить элемент списка');
	}).catch(e => {
		errorHandler(dispatch, {status: true, msg: e.message})
	})
}

export const updateStore = (item, callback) => dispatch => {
	const updateStoreUrl = new URL(`/stores/${item.id}`, url);

	fetch(updateStoreUrl.toString(), {
		method: 'PUT',
		headers: {
			'Content-type': 'application/json'
		},
		body: JSON.stringify(item)
	}).then(response => {
		if (response.status === 200) {
			dispatch(updateStoreAction(item))
			callback(true)
			return;
		}
		throw new Error('Невозможно обновить элемент списка');
	}).catch(e => {
		errorHandler(dispatch, {status: true, msg: e.message})
	})
}

