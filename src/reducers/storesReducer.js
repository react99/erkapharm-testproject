const SET_STORES = 'SET_STORES';
const DELETE_STORE = 'DELETE_STORE';
const UPDATE_STORE = 'UPDATE_STORE';
const SET_FETCH_ERROR = 'SET_FETCH_ERROR';

export const setStoresAction = payload => ({type: SET_STORES, payload});
export const deleteStoreAction = payload => ({type: DELETE_STORE, payload});
export const updateStoreAction = payload => ({type: UPDATE_STORE, payload});
export const setFetchError = payload => ({type: SET_FETCH_ERROR, payload});

const initialState = {
	storesList: [],
	fetchError: {status: false, msg: ''}
}

export const storesReducer = (state = initialState, action) => {
	switch (action.type) {
		case SET_STORES:
			return {
				...state,
				storesList: action.payload
			}
		case DELETE_STORE:
			return {
				...state,
				storesList: state.storesList.filter(i => i.id !== action.payload)
			}
			case UPDATE_STORE:
			return {
				...state,
				storesList: state.storesList.map(i => i.id === action.payload.id ? action.payload : i)
			}
			case SET_FETCH_ERROR:
			return {
				...state,
				fetchError: action.payload
			}
		default :
			return state
	}
}