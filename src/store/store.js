import {combineReducers, createStore, applyMiddleware} from "redux";
import {composeWithDevTools} from "redux-devtools-extension";
import thunk from "redux-thunk";

import {storesReducer} from "../reducers/storesReducer";

const reducer = combineReducers({
	stores: storesReducer
})

export const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)))