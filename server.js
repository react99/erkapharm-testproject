const getRandomInt = (min, max) => {
	const rnd = Math.round(min - 0.5 + Math.random() * (max - min + 1));
	return rnd < 330 ? min : max;
}

const jsonServer = require('json-server');
const server = jsonServer.create();
const router = jsonServer.router('stores.json');
const middlewares = jsonServer.defaults();

server.use(middlewares);
server.use((req, res, next) => {
	let status = getRandomInt(200, 403);
	if (req.method === 'GET') status = 200
	if (status === 200) {
		next()
	} else {
		res.sendStatus(status)
	}
})
server.use(router);

server.listen(5000, () => {
	console.log('JSON Server is running')
})